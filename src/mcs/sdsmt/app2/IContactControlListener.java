package mcs.sdsmt.app2;

import mcs.sdsmt.app2.Model.Contact;
import android.widget.ArrayAdapter;

/*********************************************************
* interface IContactControlListener
* contains the interface used by the main activity
*  
*  @author Victor Dozal, Dan Halloran
********************************************************/
public interface IContactControlListener
{
        public void selectContact(Contact contact);
        public void insertContact();
        public void insertContact(Contact contact);
        public void deleteContact(Contact contact);
        public void updateContact(Contact contact);
        public void onContactSelected(Contact contact);
        public Contact getContact();
        public ArrayAdapter<Contact> getContactArrayAdapter();
}