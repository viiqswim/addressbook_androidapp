package mcs.sdsmt.app2;

import mcs.sdsmt.app2.Model.Contact;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/*********************************************************
*
*  Class that initializes and handles the detail fragment
*	Extends:
*		Fragment
*  @author Victor Dozal, Dan Halloran
********************************************************/
public class ViewDetailFragment extends Fragment {

	
	 private IContactControlListener _listener;
     private Contact _contact = null;
     private boolean _isOrientationChanging = false;
     
     private EditText _NameText;
     private EditText _PhoneText;
     private EditText _EmailText;
     private EditText _AddressText1;
     private EditText _AddressText2;
     private Button _createButton;
     private Button _updateButton;

 	/************************************************************************
 	 * 	onCreate()
 	 * 	Retains instance and enables options menu
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Bundle savedInstanceState
 	 *  @return Nothing
 	 ***********************************************************************/
     @Override
     public void onCreate(Bundle savedInstanceState)
     {
             super.onCreate(savedInstanceState);

             // Keep member variables and state, not the best approach, 
             // but the Contact class would need to implement Parceable
             // in order to be passed in Bundle (from both outside the
             // fragment and inside the fragment on rotation).
             setRetainInstance(true);
             
             // Tells the host Activity to display the appropriate
             // option menu.
             setHasOptionsMenu(true);
     }

 	/************************************************************************
 	 * 	onCreateView()
 	 * 	initializes text boxes and buttons on fragment
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param LayoutInfator inflator, ViewGroup container, Bundle savedInstanceState
 	 *  @return rootview
 	 ***********************************************************************/
     @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
     {
             // Inflate the UI.
             View rootView = inflater.inflate(R.layout.fragment_list_detail, container, false);

             // Assign instances of Views from the Layout Resource.
             _NameText = (EditText) rootView.findViewById(R.id.NameText);
             _PhoneText = (EditText) rootView.findViewById(R.id.PhoneText);
             _EmailText = (EditText) rootView.findViewById(R.id.EmailText);
             _AddressText1 = (EditText) rootView.findViewById(R.id.AddressText1);
             _AddressText2 = (EditText) rootView.findViewById(R.id.AddressText2);
             _createButton = (Button) rootView.findViewById(R.id.createContactButton);
            _createButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					_contact.ContactName = _NameText.getText().toString();
		 			_contact.phone = _PhoneText.getText().toString();
		 			_contact.email = _EmailText.getText().toString();
		 			_contact.street = _AddressText1.getText().toString();
		 			_contact.city = _AddressText2.getText().toString();
		 			_listener.insertContact(_contact);
				}
			});
             _updateButton = (Button) rootView.findViewById(R.id.updateContactButton);
             _updateButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
		 			_contact.ContactName = _NameText.getText().toString();
		 			_contact.phone = _PhoneText.getText().toString();
		 			_contact.email = _EmailText.getText().toString();
		 			_contact.street = _AddressText1.getText().toString();
		 			_contact.city = _AddressText2.getText().toString();
		 			_listener.updateContact(_contact);
					
				}
			});

             return rootView;
     }
     
 	/************************************************************************
 	 * 	onAttach()
 	 * 	assigns listener from activity
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Activity activity
 	 *  @return Nothing
 	 ***********************************************************************/
     @Override
     public void onAttach(Activity activity)
     {
             try
             {
                     // Assign listener reference from host activity.
                     _listener = (IContactControlListener) activity;
             }
             catch (ClassCastException e)
             {
                     throw new ClassCastException(activity.toString());
             }
             
             super.onAttach(activity);
     }
     
 	/************************************************************************
 	 * 	onResume()
 	 * 	checks for orientation changing before displaying contact
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param none
 	 *  @return Nothing
 	 ***********************************************************************/
     @Override
     public void onResume()
     {
             super.onResume();

             // If we are changing orientation, use the existing _contact
             // member to populate the view.
             if (_isOrientationChanging == false)
             {
                     // Get a reference to the contact that was selected from 
                     // the list through the listener interface.
                     _contact = _listener.getContact();
             }
             displayContact();
     }
     
 	/************************************************************************
 	 * 	onPause()
 	 * 	checks if the orientation is changing
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param none
 	 *  @return Nothing
 	 ***********************************************************************/
     @Override
     public void onPause()
     {
             // Provides a mechanism by which the Fragment knows if the
             // host Activity is being re-created.  If so, we will want
             // to just use the currently selected _contact object to 
             // populate the view which is possible because we are using
             // setRetainInstance(true).
             _isOrientationChanging = getActivity().isChangingConfigurations();
             
             super.onPause();
     }

 	/************************************************************************
 	 * 	onCreateOptionsMenu()
 	 * 	displays menu only if a contact is being edited.
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Menu menu MenuInflator menuInflator
 	 *  @return Nothing
 	 ***********************************************************************/
     @Override
     public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflator)
     {
             // Only provide the host activity with a menu if there is an 
             // actual contact that is being edited.  Otherwise, it is a new
             // contact and neither of the Update or Delete menu items should
             // be available.
             if (_contact.ID > 0)
             {
                     // Inflate the menu; this adds items to the action bar if it is present.
                     getActivity().getMenuInflater().inflate(R.menu.menu_detail, menu);
             }
     }

 	/************************************************************************
 	 * 	onOptionsItemSelected()
 	 * 	checks which options menu item was selected and then do the appropriate
 	 *  action
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param MenuItem item
 	 *  @return true/false
 	 ***********************************************************************/
     @Override
     public boolean onOptionsItemSelected(MenuItem item)
     {
             switch (item.getItemId())
             {
                     case R.id.action_update_contact:
                     {       
                    	    // make text fields editable
	                 	 	_NameText.setEnabled(true);
	                 	 	_PhoneText.setEnabled(true);
	                 	 	_EmailText.setEnabled(true);
	                 	 	_AddressText1.setEnabled(true);
	                 	 	_AddressText2.setEnabled(true);
                    	 
                             _updateButton.setVisibility(View.VISIBLE);
                           
                             return true;
                     }
                     case R.id.action_delete_contact:
                     {
                             _listener.deleteContact(_contact);
                             return true;
                     }
                     default:
                     {
                             return super.onOptionsItemSelected(item);
                     }
             }
     }

 	/************************************************************************
 	 * 	displayContact()
 	 * 	fills text boxes with contact values or empties the boxes if it's a 
 	 *  new contact
 	 *  @author Victor Dozal, Dan Halloran
 	 * 	@param Context context
 	 *  @return Nothing
 	 ***********************************************************************/
     private void displayContact()
     {
             if (_contact.ID > 0)
             {
                     // Use the member Contact object to populate the view.
            	 	_NameText.setText(_contact.ContactName);
            	 	_PhoneText.setText(_contact.phone);
            	 	_EmailText.setText(_contact.email);
            	 	_AddressText1.setText(_contact.street);
            	 	_AddressText2.setText(_contact.city);
             }
             else
             {
            	 	// Clear text fields
            	 	_NameText.setText("");
            	 	_PhoneText.setText("");
            	 	_EmailText.setText("");
            	 	_AddressText1.setText("");
            	 	_AddressText2.setText("");
            	 	
            	 	// make text fields editable
            	 	_NameText.setEnabled(true);
             	 	_PhoneText.setEnabled(true);
             	 	_EmailText.setEnabled(true);
             	 	_AddressText1.setEnabled(true);
             	 	_AddressText2.setEnabled(true);
             	 	
            	 	_createButton.setVisibility(View.VISIBLE);
            	 	
             }
     }
}
