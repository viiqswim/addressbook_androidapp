package mcs.sdsmt.app2;

import mcs.sdsmt.app2.Model.Contact;
import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

/*********************************************************
*
*  Class that initializes and handles the list fragment
*	Extends:
*		ListFragment
*  @author Victor Dozal, Dan Halloran
********************************************************/
public class ViewListFragment extends ListFragment {

    private IContactControlListener _listener;

    /************************************************************************
	 * 	onAttach()
	 * 	assigns listener from activity
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Activity activity
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void onAttach(Activity activity) {

        try
        {
                // Assign listener reference from hosting activity.
                _listener = (IContactControlListener) activity;
        }
        catch (ClassCastException e)
        {
                throw new ClassCastException(activity.toString());
        }
        
		super.onAttach(activity);
	}

	/************************************************************************
	 * 	onCreate()
	 * 	enables options menu
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Bundle savedInstanceState
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
	}

	/************************************************************************
	 * 	onCreateOptionsMenu()
	 * 	Inflates the list of contacts
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Menu menu, MenuInflator inflator
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		getActivity().getMenuInflater().inflate(R.menu.menu_list, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	/************************************************************************
	 * 	onOptionsItemSelected()
	 * 	calls insert contact when options menu is selected
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param MenuItem item
	 *  @return true/false
	 ***********************************************************************/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
                case R.id.action_add_contact:
                {
                        _listener.insertContact();
                }
                default:
                {
                        return super.onOptionsItemSelected(item);
                }
        }
	}

	/************************************************************************
	 * 	onResume()
	 * 	Refreshes contact list on resume
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void onResume() {
		super.onResume();
		refreshContactList();
	}
	
	/************************************************************************
	 * 	onListItemClick()
	 * 	If valid contact is clicked, call select contact
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param ListView 1, View v, int position, long id
	 *  @return Nothing
	 ***********************************************************************/
    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
            Contact contact = null;
            
            contact = (Contact) getListAdapter().getItem(position);
            if (contact != null)
            {
                    _listener.selectContact(contact);
            }
    }

    /************************************************************************
	 * 	onCreateOptionsMenu()
	 * 	Inflates the list of contacts
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Menu menu, MenuInflator inflator
	 *  @return Nothing
	 ***********************************************************************/
    private void refreshContactList()
    {
            // Assign the adapter.
            setListAdapter(_listener.getContactArrayAdapter());        
    }

}
