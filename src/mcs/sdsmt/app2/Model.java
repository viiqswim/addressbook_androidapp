package mcs.sdsmt.app2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/*********************************************************
*
*  Class that initializes database keys and creates the table
*	Extends:
*		SQLiteOPenHelper
*  @author Victor Dozal, Dan Halloran
********************************************************/
public class Model extends SQLiteOpenHelper {

	public static final String KEY_ID = "ContactID";
	public static final String KEY_NAME = "Name";
	public static final String KEY_PHONE = "Phone";
	public static final String KEY_EMAIL = "Email";
	public static final String KEY_STREET = "Street";
	public static final String KEY_CITY = "City";

	private static final String TAG = "mcs.sdsmt.app2";

	private static final String DATABASE_NAME = "mcs.sdsmt.app2.db";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE_MYCONTACTS = "Contacts";

	private static final String TABLE_CREATE_MYCONTACTS =
			"CREATE TABLE " +
					TABLE_MYCONTACTS +
					"(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					KEY_NAME + " TEXT, " + 
					KEY_PHONE + " TEXT, " +
					KEY_EMAIL + " TEXT, " +
					KEY_STREET + " TEXT, " +
					KEY_CITY + " TEXT);";

	private SQLiteDatabase _db;
	private static Model _instance;

	/*********************************************************
	 *
	 *  Class that contains contact and contact fields
	 *  Implements:
	 *  	Comparator
	 *  @author Victor Dozal, Dan Halloran
	 ********************************************************/
	public static class Contact implements Comparator<Contact>
	{
		public Contact()
		{
			ID = -1;
		}

		public Contact(long id)
		{
			ID = id;
		}

		public long ID;
		public String ContactName;
		public String phone;
		public String email;
		public String street;
		public String city;

		// Used by ArrayAdapter to determine
		// what to display in the list.
		@Override
		public String toString() 
		{
			return ContactName;
		}

		@Override
		public int compare(Contact lhs, Contact rhs)
		{
			return lhs.ContactName.compareTo(rhs.ContactName);
		}
	}
	
	/************************************************************************
	 * 	Model()
	 * 	creates database
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Context context
	 *  @return Nothing
	 ***********************************************************************/
	public Model(Context context)
	{
		// Call the parent class and pass the actual name and version of the
		// database to be created. The version will be used in the future for
		// determine whether onUpgrade() is called from the SQLiteOpenHelper
		// extension.
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/************************************************************************
	 * 	onCreate()
	 * 	Executes the create table statement
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param SQLiteDatabase db
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		// Execute the CREATE TABLE statement defined as a const.
		db.execSQL(TABLE_CREATE_MYCONTACTS);
	}

	/************************************************************************
	 * 	onUpgrade()
	 * 	Checks what version the database is on
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param SQLiteDatabase db, int oldVersion, int newVersion
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		// If there is ever a need to upgrade the database and/or table.
		// Compare old and new versions to determine if modifications
		// to the database are necessary. Typically, this will be done with
		// ALTER TABLE or CREATE TABLE SQL statements depending on the
		// change required.

		if (newVersion == 2)
		{
			// No version 2 upgrade process yet.
		}
	}

	/************************************************************************
	 * 	getInstance()
	 * 	checks if the current instance is null or not
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Context context
	 *  @return _instance
	 ***********************************************************************/
	public static synchronized Model getInstance(Context context)
	{
		// Used to synchronize access and force singleton on the 
		// database helper object.
		if (_instance == null)
		{
			_instance = new Model(context);
		}

		return _instance;
	}

	/************************************************************************
	 * 	insertContact()
	 * 	populates all the values fields and inserts contact in database
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return id
	 ***********************************************************************/
	public long insertContact(Contact contact)
	{
		// Take parameters and pass to method to populate the
		// ContentValues data structure.
		ContentValues values = populateContentValues(contact);

		// Open the database connect, keep it close to the actual operation.
		openDBConnection();

		// Execute query to update the specified contact.
		long id = _db.insert(TABLE_MYCONTACTS, null, values);

		Log.d(TAG, "ContactID inserted = " + String.valueOf(id));

		// Close the database connection as soon as possible.
		closeDBConnection();
		return id;

	}

	/************************************************************************
	 * 	updateContact()
	 * 	Populates the contact values, and writes the values to database
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return Nothing
	 ***********************************************************************/
	public void updateContact(Contact contact)
	{
		// Take parameters and pass to method to populate the
		// ContentValues data structure.
		ContentValues values = populateContentValues(contact);

		// Open the database connect, keep it close to the actual operation.
		openDBConnection();

		// Execute query to update the specified contact.
		int rowsAffected = _db.update(TABLE_MYCONTACTS,
				values,
				KEY_ID + " = ?",
						new String[] { String.valueOf(contact.ID) });

		// Close the database connection as soon as possible.
		closeDBConnection();

		if (rowsAffected == 0)
		{
			// The contact row was not updated, what should be done?
			Log.d(TAG, "Contact not updated!");
		}
	}

	/************************************************************************
	 * 	deleteContact()
	 * 	Delete contact from database
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return Nothing
	 ***********************************************************************/
	public void deleteContact(Contact contact)
	{
		// Open the database connect, keep it close to the actual operation.
		openDBConnection();

		// Execute query to delete the specified contact.
		int rowsAffected = _db.delete(TABLE_MYCONTACTS,
				KEY_ID + " = ?",
				new String[] { String.valueOf(contact.ID) });

		// Close the database connection as soon as possible.
		closeDBConnection();

		if (rowsAffected == 0)
		{
			// The contact row was not deleted, what should be done?
			Log.d(TAG, "Contact not deleted!");
		}
	}

	/************************************************************************
	 * 	getContact()
	 * 	finds contact by id and returns the contact
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param long id
	 *  @return contact
	 ***********************************************************************/
	public Contact getContact(long id)
	{
		Contact contact = null;

		openDBConnection();

		// Return the specific contact row based on ID passed.
		// _id is required by SimpleCursorAdaptor.
		Cursor cursor = _db.query(TABLE_MYCONTACTS,
				new String[] { 
				KEY_ID, 
				KEY_NAME, 
				KEY_PHONE,
				KEY_EMAIL,
				KEY_STREET,
				KEY_CITY},
				null,
				null,
				null,
				null,
				KEY_NAME);

		if (cursor.moveToFirst())
		{
			contact = cursorToContact(cursor, true);
		}

		cursor.close();
		closeDBConnection();

		return contact;
	}

	/************************************************************************
	 * 	getContacts()
	 * 	fills contact list by iterating through the cursor
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return contacts
	 ***********************************************************************/
	public List<Contact> getContacts()
	{
		List<Contact> contacts = new ArrayList<Contact>();

		openDBConnection();

		// Query for a list of contacts.
		Cursor cursor = _db.query(TABLE_MYCONTACTS,
				new String[] {
				KEY_ID, 
				KEY_NAME, 
				KEY_PHONE,
				KEY_EMAIL,
				KEY_STREET,
				KEY_CITY},
				null,
				null,
				null,
				null,
				KEY_NAME);

		// Populate the contact List by iterating through Cursor.
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) 
		{
			Contact contact = cursorToContact(cursor, false);
			contacts.add(contact);
			cursor.moveToNext();
		}

		cursor.close();
		closeDBConnection();

		return contacts;
	}

	/************************************************************************
	 * 	openDBConnection()
	 * 	Opens connection with the database
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return Nothing
	 ***********************************************************************/
	private void openDBConnection()
	{
		// Opens connection to the database for writing specifically.
		_db = getWritableDatabase();
	}

	/************************************************************************
	 * 	closeDBConnection()
	 * 	closes the database connection
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return Nothing
	 ***********************************************************************/
	private void closeDBConnection()
	{
		if (_db != null && _db.isOpen() == true)
		{
			// Close connection to database if open.
			_db.close();
		}
	}

	/************************************************************************
	 * 	cursorToContact()
	 * 	fills the contact fields using the cursor
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Cursor cursor, Boolean flag
	 *  @return contact
	 ***********************************************************************/
	private Contact cursorToContact(Cursor cursor, Boolean flag)
	{
		// Initializes a contact with an ID
		Contact contact = new Contact(cursor.getLong(cursor.getColumnIndex(KEY_ID))); 
		
		contact.ContactName = cursor.getString(cursor.getColumnIndex(KEY_NAME));
		contact.phone = cursor.getString(cursor.getColumnIndex(KEY_PHONE)); 
		contact.email = cursor.getString(cursor.getColumnIndex(KEY_EMAIL)); 
		contact.street = cursor.getString(cursor.getColumnIndex(KEY_STREET)); 
		contact.city = cursor.getString(cursor.getColumnIndex(KEY_CITY));

		return contact;
	}

	/************************************************************************
	 * 	populateContentValues()
	 * 	fills a content value with the contact fields
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return values
	 ***********************************************************************/
	private ContentValues populateContentValues(Contact contact)
	{
		// Common function used to populate the ContentValues to be used in SQL
		// insert or update methods.

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, contact.ContactName);
		values.put(KEY_PHONE, contact.phone);
		values.put(KEY_EMAIL, contact.email);
		values.put(KEY_STREET, contact.street);
		values.put(KEY_CITY, contact.city);

		return values;
	}

}
