/**********************************************************
 * Title:
 * 		Address Book
 * Authors:
 * 		Victor Dozal, Dan Halloran
 * Class:
 * 		CSC 492, Mobile Computing
 * Instructors:
 * 		Dr. Logar
 * 		Professor Brian Butterfield
 * Date:
 * 		October 28th, 2013
 * Description:
 * 		This app allows users to save entries into a phone
 *       book, edit the entries, and delete the entries
 
 * Program Errors:
 * 		Minor rotation bugs that could be optimized
 * 
 *********************************************************/

package mcs.sdsmt.app2;

import java.util.List;

import mcs.sdsmt.app2.Model.Contact;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.widget.ArrayAdapter;


/*********************************************************
 *
 *  Contains the functions for the main flow of the app.
 *	Extends:
 *		Activity
 *  Implements:
 *  	IContactControlListener
 *  @author Victor Dozal, Dan Halloran
 ********************************************************/
public class MainActivity extends Activity implements IContactControlListener {

	
    private final static String FRAGMENT_LIST_TAG = "ContactListTag";
    private final static String FRAGMENT_DETAIL_TAG = "ContactViewTag";
	
    private FragmentManager _fragmentManager;
    private ViewListFragment _fragmentList;
    private ViewDetailFragment _fragmentDetail;
    
    private Model _model;
    private Contact _contact;
    private List<Contact> _contacts;
    private ArrayAdapter<Contact> _adapter;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
        _fragmentManager = getFragmentManager();
        
        // If the fragment is not found, create it.
        _fragmentList = (ViewListFragment) _fragmentManager.findFragmentByTag(FRAGMENT_LIST_TAG);
        if (_fragmentList == null)
        {
                _fragmentList = new ViewListFragment();
        }
        
        // If the fragment is not found, create it.
        _fragmentDetail = (ViewDetailFragment) _fragmentManager.findFragmentByTag(FRAGMENT_DETAIL_TAG);
        if (_fragmentDetail == null)
        {
                _fragmentDetail = new ViewDetailFragment();
        }
        
        // Only add/replace the list fragment if the bundle is empty; otherwise,
        // the activity is being re-created so keep the fragment that is already
        // displayed.
        if (savedInstanceState == null)
        {
                _fragmentManager.beginTransaction()
                                .replace(R.id.fragmentContainerFrame, _fragmentList, FRAGMENT_LIST_TAG)
                                .commit();
        }
        
        // Get single instance to the model to handle
        // all database activity.
        _model = Model.getInstance(this);
                
        refreshArrayAdapter();
	}
	
	/************************************************************************
	 * 	selectContact()
	 * 	Sets current contact as selected and switches to detail fragment
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void selectContact(Contact contact) {
		_contact = contact;
		showDetailFragment();
		
	}

	/************************************************************************
	 * 		insertContact()
	 * 		Creates new contact and switches to detail fragment
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void insertContact()
	{
		_contact = new Contact();
		showDetailFragment();
		
	}

	/************************************************************************
	 * 		insertContact()
	 * 		Adds contact passed to function to array adapter, sorts the list
	 * 		and then refreshes the adapter
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void insertContact(Contact contact) {
		        
        contact.ID = _model.insertContact(contact);
        
        _adapter.add(contact);
        _adapter.sort(contact);
        _adapter.notifyDataSetChanged();
        
        _fragmentManager.popBackStackImmediate();
        refreshArrayAdapter();
		
	}

	/************************************************************************
	 * 		deleteContact()
	 * 		removes the contact from the array adapter and sorts the array
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void deleteContact(Contact contact) {
		
	      _adapter.remove(contact);
          _adapter.sort(contact);
          _adapter.notifyDataSetChanged();
          
          _model.deleteContact(contact);
          _fragmentManager.popBackStackImmediate();		
	}

	/************************************************************************
	 * 		updateContact()
	 * 		removes and re-adds contact to array adapter, then sorts
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void updateContact(Contact contact) {
		
        _adapter.remove(contact);
        _adapter.add(contact);
        _adapter.sort(contact);
        _adapter.notifyDataSetChanged();
        
        _model.updateContact(contact);
        _fragmentManager.popBackStackImmediate();		
	}

	/************************************************************************
	 * 		getContact()
	 * 		returns the current contact
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return _contact
	 ***********************************************************************/
	@Override
	public Contact getContact() {
		
		return _contact;
	}

	/************************************************************************
	 * 		getContactArrayAdapter()
	 * 		returns current adapter
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return _adapter
	 ***********************************************************************/
	@Override
	public ArrayAdapter<Contact> getContactArrayAdapter() {
		
		return _adapter;
	}

	/************************************************************************
	 * 		onContactSelected()
	 * 		required override function
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param Contact contact
	 *  @return Nothing
	 ***********************************************************************/
	@Override
	public void onContactSelected(Contact contact) {
		
	}
	
	/************************************************************************
	 * 		refreshArrayAdpater()
	 * 		reassigns contacts and array adapter
	 *  @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return Nothing
	 ***********************************************************************/
    private void refreshArrayAdapter()
    {
            // Get an Array List of Contact objects.
            _contacts = Model.getInstance(this).getContacts();
            
            // Assign list to ArrayAdapter to be used with assigning
            // to the ListFragment list adapter.
            _adapter = new ArrayAdapter<Contact>(this,
                                                 android.R.layout.simple_list_item_1, 
                                                 _contacts);
    }
    
	/************************************************************************
	 * 		showDetailFragment()
	 * 		When the MainActivity is created, it renders everything.
	 * @author Victor Dozal, Dan Halloran
	 * 	@param none
	 *  @return Nothing
	 ***********************************************************************/
	private void showDetailFragment() 
	{
        // Perform the fragment transaction to display the details fragment.
        _fragmentManager.beginTransaction()
                                .replace(R.id.fragmentContainerFrame, _fragmentDetail, FRAGMENT_DETAIL_TAG)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .addToBackStack(null)
                                .commit();
	}

	
}
